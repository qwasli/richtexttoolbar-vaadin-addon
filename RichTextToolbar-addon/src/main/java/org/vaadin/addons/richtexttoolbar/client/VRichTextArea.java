/*
 * Copyright 2000-2013 Vaadin Ltd.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

package org.vaadin.addons.richtexttoolbar.client;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.function.Consumer;

import org.vaadin.addons.richtexttoolbar.client.richtexttoolbar.RichTextToolbarConnector;

import com.google.gwt.core.client.Scheduler;
import com.google.gwt.dom.client.BodyElement;
import com.google.gwt.dom.client.Document;
import com.google.gwt.dom.client.Element;
import com.google.gwt.dom.client.HeadElement;
import com.google.gwt.dom.client.IFrameElement;
import com.google.gwt.dom.client.LinkElement;
import com.google.gwt.dom.client.NativeEvent;
import com.google.gwt.dom.client.Node;
import com.google.gwt.dom.client.NodeList;
import com.google.gwt.event.dom.client.BlurHandler;
import com.google.gwt.event.dom.client.KeyDownEvent;
import com.google.gwt.event.dom.client.KeyDownHandler;
import com.google.gwt.event.dom.client.KeyPressEvent;
import com.google.gwt.event.dom.client.KeyPressHandler;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.user.client.Command;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Focusable;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HasEnabled;
import com.google.gwt.user.client.ui.RichTextArea;
import com.google.gwt.user.client.ui.RichTextArea.Formatter;
import com.google.gwt.user.client.ui.Widget;
import com.vaadin.client.ApplicationConnection;
import com.vaadin.client.BrowserInfo;
import com.vaadin.client.ComponentConnector;
import com.vaadin.client.ConnectorMap;
import com.vaadin.client.ui.Field;
import com.vaadin.client.ui.ShortcutActionHandler;
import com.vaadin.client.ui.ShortcutActionHandler.ShortcutActionHandlerOwner;
import com.vaadin.client.ui.TouchScrollDelegate;
import com.vaadin.client.ui.dd.DragImageModifier;
import com.vaadin.shared.Connector;

/**
 * This class implements a basic client side rich text editor component.
 * 
 * @author Vaadin Ltd.
 * 
 */
public class VRichTextArea extends Composite implements Field, KeyPressHandler,
        KeyDownHandler, Focusable, HasEnabled, DragImageModifier {

    /**
     * The input node CSS classname.
     */
    public static final String CLASSNAME = "v-richtextarea";

    /** For internal use only. May be removed or replaced in the future. */
    public String id;

    /** For internal use only. May be removed or replaced in the future. */
    public ApplicationConnection client;

    /** For internal use only. May be removed or replaced in the future. */
    public boolean immediate = false;

    /** For internal use only. May be removed or replaced in the future. */
    public RichTextArea rta;

    /** For internal use only. May be removed or replaced in the future. */
    public VRichTextToolbar formatter;

    /** For internal use only. May be removed or replaced in the future. */
    public boolean internalFormatter = false;

    /** For internal use only. May be removed or replaced in the future. */
    public HTML html = new HTML();

    private final FlowPanel fp = new FlowPanel();

    private boolean enabled = true;

    /** For internal use only. May be removed or replaced in the future. */
    public int maxLength = -1;

    private int toolbarNaturalWidth = 500;

    /** For internal use only. May be removed or replaced in the future. */
    public HandlerRegistration keyPressHandler;

    private ShortcutActionHandlerOwner hasShortcutActionHandler;

    private boolean readOnly = false;

    public boolean autoGrowWidth = false;
    public boolean autoGrowHeight = false;

    private boolean initialised = false;

    private int lastAutoHeight = 0;

    private final Map<BlurHandler, HandlerRegistration> blurHandlers = new HashMap<BlurHandler, HandlerRegistration>();

    private List<Command> inputHandlers = new ArrayList<>();

    public VRichTextArea() {
        createRTAComponents();
        initWidget(fp);
        setStyleName(CLASSNAME);
        fp.add(rta);
        TouchScrollDelegate.enableTouchScrolling(html, html.getElement());
    }

    private void createRTAComponents() {
        if (formatter != null)
            formatter.detachVRTA(this);
        rta = new RichTextArea();
        rta.addInitializeHandler(e -> {
            initialised = true;
            IFrameElement fe = IFrameElement.as(rta.getElement());
            final Document cd = fe.getContentDocument();
            if (cd == null)
                return;
            else {

                HeadElement head = cd.getHead();
                LinkElement linkElement = cd.createLinkElement();
                linkElement.setRel("stylesheet");
                linkElement
                        .setHref("./VAADIN/addons/richtexttoolbar/styles.css");
                linkElement.setType("text/css");
                final HeadElement fHhead = head;
                fHhead.appendChild(linkElement);

                BodyElement body = cd.getBody();
                addInputListener(body, event -> {
                    inputHandlers.forEach(handler -> handler.execute());
                });
                addDropListener(cd, event -> {
                    event.preventDefault();
                    event.stopPropagation();
                });
            }

            if (autoGrowHeight) {
                new Timer() {
                    @Override
                    public void run() {
                        setAutoHeight();
                    }
                }.schedule(320);
            }
            if (autoGrowWidth) {
                new Timer() {

                    @Override
                    public void run() {
                        setAutoWidth();
                    }
                }.schedule(320);
            }

            rta.addKeyDownHandler(VRichTextArea.this);

            if (formatter != null)
                formatter.attachVRTA(VRichTextArea.this);
            // Add blur handlers
            for (

            Entry<BlurHandler, HandlerRegistration> handler : blurHandlers
                    .entrySet()) {

                // Remove old registration
                handler.getValue().removeHandler();

                // Add blur handlers
                addBlurHandler(handler.getKey());
            }

        });
        rta.setWidth("100%");
    }

    private native void addInputListener(Element element,
            Consumer<NativeEvent> listener)
    /*-{
        element.addEventListener("input", $entry(function(event) {
            listener.@java.util.function.Consumer::accept(Ljava/lang/Object;)(event);
        }));
    }-*/;

    private native void addDropListener(Document doc,
            Consumer<NativeEvent> listener)
    /*-{
        doc.addEventListener("drop", $entry(function(event) {
            listener.@java.util.function.Consumer::accept(Ljava/lang/Object;)(event);
        }));
    }-*/;

    public void setMaxLength(int maxLength) {
        if (maxLength >= 0) {
            if (keyPressHandler == null) {
                keyPressHandler = rta.addKeyPressHandler(this);
            }
            this.maxLength = maxLength;
        } else if (this.maxLength != -1) {
            this.maxLength = -1;
            removeKeyPressHandlerIfNotNeeded();
        }

    }

    @Override
    public void setEnabled(boolean enabled) {
        if (this.enabled != enabled) {
            // rta.setEnabled(enabled);
            swapEditableArea();
            this.enabled = enabled;
        }
    }

    @Override
    public boolean isEnabled() {
        return enabled;
    }

    /**
     * Swaps html to rta and visa versa.
     */
    private void swapEditableArea() {
        String value = getValue();
        if (html.isAttached()) {
            fp.remove(html);
            if (BrowserInfo.get().isWebkit()) {
                if (internalFormatter)
                    fp.remove(formatter);
                createRTAComponents(); // recreate new RTA to bypass #5379
                if (internalFormatter)
                    fp.add(formatter);
            }
            fp.add(rta);
        } else {
            fp.remove(rta);
            fp.add(html);
        }
        setValue(value);
    }

    /** For internal use only. May be removed or replaced in the future. */
    public void selectAll() {
        /*
         * There is a timing issue if trying to select all immediately on first
         * render. Simple deferred command is not enough. Using Timer with
         * moderated timeout. If this appears to fail on many (most likely slow)
         * environments, consider increasing the timeout.
         * 
         * FF seems to require the most time to stabilize its RTA. On Vaadin
         * tiergarden test machines, 200ms was not enough always (about 50%
         * success rate) - 300 ms was 100% successful. This however was not
         * enough on a sluggish old non-virtualized XP test machine. A bullet
         * proof solution would be nice, GWT 2.1 might however solve these. At
         * least setFocus has a workaround for this kind of issue.
         */
        new Timer() {
            @Override
            public void run() {
                rta.getFormatter().selectAll();
            }
        }.schedule(320);
    }

    public void setReadOnly(boolean b) {
        if (isReadOnly() != b) {
            swapEditableArea();
            readOnly = b;
        }
        // reset visibility in case enabled state changed and the formatter was
        // recreated
        if (internalFormatter)
            formatter.setVisible(!readOnly);
    }

    private boolean isReadOnly() {
        return readOnly;
    }

    @Override
    public void setHeight(String height) {
        super.setHeight(height);

        if (height == null || height.equals("")) {
            rta.setHeight("");
        }
    }

    @Override
    public void setWidth(String width) {
        if (width.equals("")) {
            /*
             * IE cannot calculate the width of the 100% iframe correctly if
             * there is no width specified for the parent. In this case we would
             * use the toolbar but IE cannot calculate the width of that one
             * correctly either in all cases. So we end up using a default width
             * for a RichTextArea with no width definition in all browsers (for
             * compatibility).
             */

            super.setWidth(toolbarNaturalWidth + "px");
        } else {
            super.setWidth(width);
        }
    }

    @Override
    public void onKeyPress(KeyPressEvent event) {
        if (maxLength >= 0) {
            Scheduler.get().scheduleDeferred(new Command() {
                @Override
                public void execute() {
                    if (rta.getHTML().length() > maxLength) {
                        rta.setHTML(rta.getHTML().substring(0, maxLength));
                    }
                }
            });
        }
        if (autoGrowWidth) {
            Scheduler.get().scheduleDeferred(new Command() {

                @Override
                public void execute() {
                    setAutoWidth();
                }
            });
        }
        if (autoGrowHeight) {
            Scheduler.get().scheduleDeferred(new Command() {
                @Override
                public void execute() {
                    setAutoHeight();
                }
            });
        }

    }

    @Override
    public void onKeyDown(KeyDownEvent event) {
        // delegate to closest shortcut action handler
        // throw event from the iframe forward to the shortcuthandler
        ShortcutActionHandler shortcutHandler = getShortcutHandlerOwner()
                .getShortcutActionHandler();
        if (shortcutHandler != null) {
            shortcutHandler.handleKeyboardEvent(
                    com.google.gwt.user.client.Event.as(event.getNativeEvent()),
                    ConnectorMap.get(client).getConnector(this));
        }
    }

    private ShortcutActionHandlerOwner getShortcutHandlerOwner() {
        if (hasShortcutActionHandler == null) {
            Widget parent = getParent();
            while (parent != null) {
                if (parent instanceof ShortcutActionHandlerOwner) {
                    break;
                }
                parent = parent.getParent();
            }
            hasShortcutActionHandler = (ShortcutActionHandlerOwner) parent;
        }
        return hasShortcutActionHandler;
    }

    @Override
    public int getTabIndex() {
        return rta.getTabIndex();
    }

    @Override
    public void setAccessKey(char key) {
        rta.setAccessKey(key);
    }

    @Override
    public void setFocus(final boolean focused) {
        /*
         * Similar issue as with selectAll. Focusing must happen before possible
         * selectall, so keep the timeout here lower.
         */
        new Timer() {

            @Override
            public void run() {
                BrowserInfo bi = BrowserInfo.get();
                if (focused && (bi.isChrome() || bi.isSafari()))
                    rta.getElement().focus();
                rta.setFocus(focused);
            }
        }.schedule(300);
    }

    @Override
    public void setTabIndex(int index) {
        rta.setTabIndex(index);
    }

    /**
     * Set the value of the text area
     * 
     * @param value
     *            The text value. Can be html.
     */
    public void setValue(String value) {
        if (rta.isAttached()) {
            rta.setHTML(value);
            if (initialised) {
                if (autoGrowWidth) {
                    scheduleAutoWidth();
                }
                if (autoGrowHeight) {
                    scheduleAutoHeight();
                }
            }
        } else {
            html.setHTML(value);
        }
    }

    private void scheduleAutoWidth() {
        Scheduler.get().scheduleDeferred(new Command() {
            @Override
            public void execute() {
                setAutoWidth();
            }
        });
    }

    private void scheduleAutoHeight() {
        Scheduler.get().scheduleDeferred(new Command() {
            @Override
            public void execute() {
                setAutoHeight();
            }
        });
    }

    /**
     * Get the value the text area
     */
    public String getValue() {
        if (isAttached() && rta.isAttached()) {
            return rta.getHTML();
        } else {
            return html.getHTML();
        }
    }

    /**
     * Browsers differ in what they return as the content of a visually empty
     * rich text area. This method is used to normalize these to an empty
     * string. See #8004.
     * 
     * @return cleaned html string
     */
    public String getSanitizedValue() {
        BrowserInfo browser = BrowserInfo.get();
        String result = getValue();
        if (browser.isFirefox()) {
            if ("<br>".equals(result)) {
                result = "";
            }
            if (!result.isEmpty() && result.endsWith("<br>")
                    && !result.endsWith("<br><br>")) {
                result = result.substring(0, result.length() - 4);
            }
        } else if (browser.isWebkit()) {
            if ("<br>".equals(result) || "<div><br></div>".equals(result)) {
                result = "";
            }
        } else if (browser.isIE()) {
            if ("<P>&nbsp;</P>".equals(result)) {
                result = "";
            }
        } else if (browser.isOpera()) {
            if ("<br>".equals(result) || "<p><br></p>".equals(result)) {
                result = "";
            }
        }
        return result;
    }

    /**
     * Adds a blur handler to the component.
     * 
     * @param blurHandler
     *            the blur handler to add
     */
    public void addBlurHandler(BlurHandler blurHandler) {
        blurHandlers.put(blurHandler, rta.addBlurHandler(blurHandler));
    }

    /**
     * Removes a blur handler.
     * 
     * @param blurHandler
     *            the handler to remove
     */
    public void removeBlurHandler(BlurHandler blurHandler) {
        HandlerRegistration registration = blurHandlers.remove(blurHandler);
        if (registration != null) {
            registration.removeHandler();
        }
    }

    public HandlerRegistration addInputHandler(Command inputHandler) {
        inputHandlers.add(inputHandler);
        return () -> inputHandlers.remove(inputHandler);
    }

    public Formatter getRTAFormatter() {
        return rta.getFormatter();
    }

    public RichTextArea getRTA() {
        return rta;
    }

    public void setToolbar(Connector toolbarC) {
        if (toolbarC == null) {
            this.internalFormatter = true;
            if (formatter != null)
                formatter.detachVRTA(this);

            formatter = new VRichTextToolbar();
            if (!isReadOnly()) {
                fp.remove(rta);
                fp.add(formatter);
                fp.add(rta);
                formatter.attachVRTA(this);
            }
        } else if (toolbarC != null) {
            this.internalFormatter = false;

            if (formatter != null) {
                fp.remove(formatter);
                formatter.detachVRTA(this);
            }

            RichTextToolbarConnector rttc = (RichTextToolbarConnector) toolbarC;
            VRichTextToolbar toolbar = rttc.getWidget();

            this.formatter = toolbar;
            this.formatter.attachVRTA(this);

        }
    }

    public void setAutoGrowHeight(boolean autoGrowHeight) {
        this.autoGrowHeight = autoGrowHeight;
        if (this.autoGrowHeight && keyPressHandler == null)
            keyPressHandler = rta.addKeyPressHandler(this);
        if (!this.autoGrowHeight)
            removeKeyPressHandlerIfNotNeeded();
        else
            scheduleAutoHeight();
    }

    public void setAutoGrowWidth(boolean autoGrowWidth) {
        this.autoGrowWidth = autoGrowWidth;
        if (this.autoGrowWidth && keyPressHandler == null)
            keyPressHandler = rta.addKeyPressHandler(this);
        if (!this.autoGrowWidth)
            removeKeyPressHandlerIfNotNeeded();
        else
            scheduleAutoWidth();
    }


    // // TODO find better way than the ugly hack with catch and forget.
    // @Override
    // protected void onUnload() {
    // if (!readOnly) {
    // if (internalFormatter)
    // fp.remove(formatter);
    // try {
    // fp.remove(rta);
    // } catch (Exception e) {
    // }
    // }
    // if (formatter != null)
    // formatter.detachVRTA(this);
    // super.onUnload();
    // }
    //
    // @Override
    // protected void onLoad() {
    // super.onLoad();
    // if (formatter != null)
    // formatter.attachVRTA(this);
    // if (!readOnly) {
    // // rta.setHTML(getValue());
    // if (internalFormatter)
    // fp.add(formatter);
    // fp.add(rta);
    // }
    // }

    void setAutoWidth() {
        IFrameElement fe = IFrameElement.as(rta.getElement());
        Document contentDocument = fe.getContentDocument();
        int w = contentDocument.getBody().getOffsetWidth();

        if (internalFormatter && w < formatter.getOffsetWidth())
            w = formatter.getOffsetWidth();
        setWidth(w + "px");
    }

    void setAutoHeight() {

        if (!isAttached() || !rta.isAttached())
            return;

        IFrameElement fe = IFrameElement.as(rta.getElement());
        Document contentDocument = fe.getContentDocument();
        BodyElement body = contentDocument.getBody();

        int boh = body.getOffsetHeight();
        int oboH = 0;
        int maxTurns = 100;

        while (oboH != boh && maxTurns > 0) {
            rta.setHeight(boh + "px");
            oboH = boh;
            boh = body.getOffsetHeight();
            maxTurns--;
        }
        final int sh = body.getScrollHeight();
        rta.setHeight(sh + "px");
        if (lastAutoHeight != sh) {
            final ComponentConnector con = ConnectorMap.get(client)
                    .getConnector(this);
            con.getLayoutManager().setNeedsMeasure(con);
        }
        lastAutoHeight = sh;
    }

    private boolean needsKeyPressHandler() {
        return autoGrowHeight || autoGrowWidth || maxLength >= 0;
    }

    private void removeKeyPressHandlerIfNotNeeded() {
        if (keyPressHandler == null || needsKeyPressHandler())
            return;
        keyPressHandler.removeHandler();
        keyPressHandler = null;
    }

    @Override
    public void modifyDragImage(Element element) {
        final NodeList<Node> childNodes = element.getChildNodes();
        for (int i = 0; i < childNodes.getLength(); i++) {
            final Node cN = childNodes.getItem(i);
            if ("IFRAME".equalsIgnoreCase(cN.getNodeName())) {
                html.setHTML(rta.getHTML());
                element.replaceChild(html.getElement().cloneNode(true), cN);
            }
        }
    }

}
