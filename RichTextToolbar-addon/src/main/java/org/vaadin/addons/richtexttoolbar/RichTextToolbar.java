package org.vaadin.addons.richtexttoolbar;

import org.vaadin.addons.richtexttoolbar.client.richtexttoolbar.RichTextToolbarState;

// This is the server-side UI component that provides public API 
// for RichTextToolbar
public class RichTextToolbar extends com.vaadin.ui.AbstractComponent {

    public RichTextToolbar() {}

    @Override
    public RichTextToolbarState getState() {
        return (RichTextToolbarState) super.getState();
    }

    boolean singleLinePanel = false;

    public boolean isSingleLinePanel() {
        return singleLinePanel;
    }

    public void setSingleLinePanel(boolean singleLinePanel) {
        this.singleLinePanel = singleLinePanel;
        getState().singleLinePanel = this.singleLinePanel;
    }

    public void setColorNames(String white, String black, String red,
            String green, String yellow, String blue) {
        getState().colorNames[0] = white;
        getState().colorNames[1] = black;
        getState().colorNames[2] = red;
        getState().colorNames[3] = green;
        getState().colorNames[4] = yellow;
        getState().colorNames[5] = blue;
    }
}
