package org.vaadin.addons.richtexttoolbar.client.richtexttoolbar;

public class RichTextToolbarState
        extends com.vaadin.shared.AbstractComponentState {

    public boolean singleLinePanel = false;
    public String[] colorNames = new String[] { "white", "black", "red",
            "green", "yellow", "blue" };
}