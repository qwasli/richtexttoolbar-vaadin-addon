package org.vaadin.addons.richtexttoolbar.demo;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.servlet.annotation.WebServlet;

import org.vaadin.addons.richtexttoolbar.RichTextArea;
import org.vaadin.addons.richtexttoolbar.RichTextToolbar;

import com.vaadin.annotations.Theme;
import com.vaadin.annotations.Title;
import com.vaadin.annotations.VaadinServletConfiguration;
import com.vaadin.annotations.Widgetset;
import com.vaadin.event.ShortcutAction.KeyCode;
import com.vaadin.event.ShortcutAction.ModifierKey;
import com.vaadin.event.Transferable;
import com.vaadin.event.dd.DragAndDropEvent;
import com.vaadin.event.dd.DropHandler;
import com.vaadin.event.dd.DropTarget;
import com.vaadin.event.dd.TargetDetails;
import com.vaadin.event.dd.acceptcriteria.AcceptCriterion;
import com.vaadin.event.dd.acceptcriteria.Not;
import com.vaadin.event.dd.acceptcriteria.SourceIsTarget;
import com.vaadin.server.VaadinRequest;
import com.vaadin.server.VaadinServlet;
import com.vaadin.shared.ui.dd.HorizontalDropLocation;
import com.vaadin.ui.AbstractOrderedLayout;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.Component;
import com.vaadin.ui.CustomComponent;
import com.vaadin.ui.DragAndDropWrapper;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Layout;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;

@Title("RichTextToolbar Add-on Demo")
@SuppressWarnings("serial")
@Widgetset("org.vaadin.addons.richtexttoolbar.DemoWidgetset")
@Theme("demo")
public class DemoUI extends UI {
    private static final String LOREM = "<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. <b>Quae sequuntur igitur?</b> Quis istud possit, inquit, negare? <mark>Equidem e Cn.</mark> Sed ad rem redeamus; <b>Sed residamus, inquit, si placet.</b> </p>"
            + "<p><b>Duo Reges: constructio interrete.</b> Ratio enim nostra consentit, pugnat oratio. An nisi populari fama? Immo videri fortasse. Et quidem, inquit, vehementer errat; Moriatur, inquit. </p>"
            + "<p>Vide, quantum, inquam, fallare, Torquate. <b>Inquit, dasne adolescenti veniam?</b> <mark>Quid censes in Latino fore?</mark> </p>"
            + "<p>De illis, cum volemus. Si longus, levis. Eam stabilem appellas. Erit enim mecum, si tecum erit. </p>"
            + "<p>Quo igitur, inquit, modo? Sed ad illum redeo. Praeclarae mortes sunt imperatoriae; </p>";

    private List<RichTextArea> rtas = new ArrayList<RichTextArea>();
    HorizontalLayout buttons = new HorizontalLayout();
    SortableLayout areas = new SortableLayout();
    RichTextToolbar rtt = new RichTextToolbar();

    @Override
    protected void init(VaadinRequest request) {
        setContent(new VerticalLayout(buttons, rtt, areas));
        addButtons(buttons);
        addAreas(rtt, areas, 1);
        rtt.setColorNames("Wiis", "Schwarz", "Rot", "Grüen", "Gälb", "Blau");
    }

    private void addButtons(Layout layout) {
        Button b = new Button("Toggle toolbar single line",
                new ClickListener() {

                    @Override
                    public void buttonClick(ClickEvent event) {
                        rtt.setSingleLinePanel(!rtt.isSingleLinePanel());
                    }
                });
        layout.addComponent(b);
        b.setClickShortcut(KeyCode.T, ModifierKey.CTRL);
        b = new Button("Toggle toolbar connection", new ClickListener() {

            @Override
            public void buttonClick(ClickEvent event) {
                for (RichTextArea rta : rtas) {
                    if (rta.getToolbar() == null)
                        rta.setToolbar(rtt);
                    else
                        rta.setToolbar(null);
                }
            }
        });
        layout.addComponent(b);

        b = new Button("Toggle autoGrowHeight", new ClickListener() {

            @Override
            public void buttonClick(ClickEvent event) {
                for (RichTextArea rta : rtas) {
                    rta.setAutoGrowHeight(!rta.isAutoGrowHeight());
                }
            }
        });
        b.setClickShortcut(KeyCode.G, ModifierKey.CTRL);
        layout.addComponent(b);
        b = new Button("Toggle Readonly", new ClickListener() {

            @Override
            public void buttonClick(ClickEvent event) {
                for (RichTextArea rta : rtas)
                    rta.setReadOnly(!rta.isReadOnly());
            }
        });
        b.setClickShortcut(KeyCode.R, ModifierKey.CTRL);
        layout.addComponent(b);
        b = new Button("Reset value", new ClickListener() {

            @Override
            public void buttonClick(ClickEvent event) {
                for (RichTextArea rta : rtas)
                    rta.setValue(LOREM);
            }
        });
        b.setClickShortcut(KeyCode.ENTER, ModifierKey.CTRL);
        layout.addComponent(b);
        
        b = new Button("Add area", new ClickListener() {

            @Override
            public void buttonClick(ClickEvent event) {
                addAreas(rtt, areas, 1);
            }
        });
        b.setClickShortcut(KeyCode.ENTER, ModifierKey.CTRL);
        layout.addComponent(b);
        b = new Button("Add bar & rta", new ClickListener() {

            @Override
            public void buttonClick(ClickEvent event) {
                RichTextToolbar bar = new RichTextToolbar();
                areas.addComponent(bar);
                addAreas(bar, areas, 1);
            }
        });
        b.setClickShortcut(KeyCode.ENTER, ModifierKey.CTRL);
        layout.addComponent(b);

    }

    private void addAreas(RichTextToolbar rtt, SortableLayout sLayout,
            int num) {
        for (int i = 0; i < num; i++) {
            System.out.println("Adding RTA " + i);
            RichTextArea rta = new RichTextArea("RTA " + i);
            rta.setReadOnly(false);
            // rta.setAutoGrowHeight(true);
            rta.setValue(LOREM);
            // rta.setAutoGrowHeight(true);
            rta.setToolbar(rtt);
            sLayout.addComponent(rta);
            this.rtas.add(rta);
        }

    }

    private static class SortableLayout extends CustomComponent {
        private final AbstractOrderedLayout layout;
        private final DropHandler dropHandler;

        public SortableLayout() {
            layout = new VerticalLayout();
            dropHandler = new ReorderLayoutDropHandler(layout);

            final DragAndDropWrapper pane = new DragAndDropWrapper(layout);
            setCompositionRoot(pane);
            // setCompositionRoot(layout);
        }

        public void addComponent(final Component component) {
            layout.addComponent(component);

            final WrappedComponent wrapper = new WrappedComponent(component,
                    dropHandler);
            wrapper.setSizeUndefined();
            wrapper.setStyleName("moveable");
            component.setHeight("100%");
            wrapper.setHeight("100%");
            layout.addComponent(wrapper);
        }
    }

    private static class WrappedComponent extends DragAndDropWrapper {

        private final DropHandler dropHandler;

        public WrappedComponent(final Component content,
                final DropHandler dropHandler) {
            super(content);
            this.dropHandler = dropHandler;
            setDragStartMode(DragStartMode.WRAPPER);
        }

        @Override
        public DropHandler getDropHandler() {
            return dropHandler;
        }

    }

    private static class ReorderLayoutDropHandler implements DropHandler {

        private final AbstractOrderedLayout layout;

        public ReorderLayoutDropHandler(final AbstractOrderedLayout layout) {
            this.layout = layout;
        }

        @Override
        public AcceptCriterion getAcceptCriterion() {
            return new Not(SourceIsTarget.get());
        }

        @Override
        public void drop(final DragAndDropEvent dropEvent) {
            final Transferable transferable = dropEvent.getTransferable();
            final Component sourceComponent = transferable.getSourceComponent();
            if (sourceComponent instanceof WrappedComponent) {
                final TargetDetails dropTargetData = dropEvent
                        .getTargetDetails();
                final DropTarget target = dropTargetData.getTarget();

                // find the location where to move the dragged component
                boolean sourceWasAfterTarget = true;
                int index = 0;
                final Iterator<Component> componentIterator = layout.iterator();
                Component next = null;
                while (next != target && componentIterator.hasNext()) {
                    next = componentIterator.next();
                    if (next != sourceComponent) {
                        index++;
                    } else {
                        sourceWasAfterTarget = false;
                    }
                }
                if (next == null || next != target) {
                    // component not found - if dragging from another layout
                    return;
                }

                // drop on top of target?
                if (dropTargetData.getData("horizontalLocation")
                        .equals(HorizontalDropLocation.CENTER.toString())) {
                    if (sourceWasAfterTarget) {
                        index--;
                    }
                }

                // drop before the target?
                else if (dropTargetData.getData("horizontalLocation")
                        .equals(HorizontalDropLocation.LEFT.toString())) {
                    index--;
                    if (index < 0) {
                        index = 0;
                    }
                }

                // move component within the layout
                layout.removeComponent(sourceComponent);
                layout.addComponent(sourceComponent, index);
            }
        }
    }

    @WebServlet(value = "/*", asyncSupported = true)
    @VaadinServletConfiguration(productionMode = false, ui = DemoUI.class)
    public static class Servlet extends VaadinServlet {
    }
}
